//
//  Receipt.swift
//  202ME1
//
//  Created by Jean Manas on 10/23/21.
//

import Foundation

struct Receipt {
    let items: [Item]
    let cashPayment: Double
    let discount: Double
    
    /// Sum of all prices
    var total: Double {
        var total: Double = 0
        
        for i in items {
            total += i.price
        }
        
        return total
    }
    
    /// Total of amount to pay after subtracting discount
    var toPay: Double {
        return total - discount
    }
    
    var change: Double {
        return cashPayment - toPay
    }
    
    func printDetails() {
        print("")
        print("---------------------------")
        print("THIS IS NOT AN OFFICIAL RECEIPT\n")
        print("ITEMS ....... PRICE")

        for i in items {
            i.printDetails()
        }
        
        print("")
        print("DISCOUNT ... P\(discount)")
        print("TOTAL ...... P\(toPay)")
        print("CASH ....... P\(cashPayment)")
        print("CHANGE ..... P\(change)")
        print("---------------------------")
    }
}
