//
//  Data.swift
//  202ME1
//
//  Created by Jean Manas on 10/23/21.
//

import Foundation

struct Data {
    static let food = [
        Food(id: 0, name: "Chicken pot pie", size: .meal(450)),
        Food(id: 0, name: "Mashed potatoes", size: .solo(120)),
        Food(id: 0, name: "Lemon flavored Fried Chicken w/ Broccoli & Bean Sprouts", size: .meal(690)),
        Food(id: 0, name: "Vegan Burger", size: .solo(420)),
        Food(id: 0, name: "Pad Thai Noodle", size: .solo(260)),
        Food(id: 0, name: "Meatloaf", size: .meal(575)),
        Food(id: 0, name: "Lasagna", size: .solo(335)),
        Food(id: 0, name: "Spaghetti with meatballs", size: .meal(780)),
        Food(id: 0, name: "Mac & cheese (with an option to add bacon)", size: .solo(625)),
        Food(id: 0, name: "Giant chocolate chip cookies", size: .solo(455)),
        Food(id: 0, name: "Strawberry shortcake", size: .solo(950))
    ]
    
    static let beverage = [
        Beverage(id: 0, name: "Strawberry Acai Starbucks Refreshers", size: .small(155)),
        Beverage(id: 0, name: "Pink Drink", size: .small(120)),
        Beverage(id: 0, name: "Iced White Chocolate Mocha", size: .med(190)),
        Beverage(id: 0, name: "Iced Apple Crisp Macchiato", size: .med(230)),
        Beverage(id: 0, name: "Salted Caramel Cream Cold Brew", size: .med(210)),
        Beverage(id: 0, name: "Mocha Cookie Crumble Frappuccino", size: .med(250)),
        Beverage(id: 0, name: "Caramel Ribbon Crunch Frappuccino", size: .large(325)),
        Beverage(id: 0, name: "Mango Dragonfruit Starbucks Refreshers", size: .large(310)),
        Beverage(id: 0, name: " Kiwi Starfruit Starbucks Refreshers", size: .large(295)),
        Beverage(id: 0, name: "Dark Roast", size: .large(255)),
        Beverage(id: 0, name: "Vanilla Bean Crème Frappuccino", size: .large(300))
    ]
}
