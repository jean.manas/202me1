//
//  Item.swift
//  202ME1
//
//  Created by Jean Manas on 10/23/21.
//

import Foundation

protocol Item {
    var id: Int { get set }
    var name: String { get set }
    var sizeStr: String { get }
    var price: Double { get }
    
    func printDetails()
}

extension Item where Self: Any {
    func printDetails() {
        print("\(name) (\(sizeStr)) .... P\(price)")
    }
}
