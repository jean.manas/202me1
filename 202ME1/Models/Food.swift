//
//  Food.swift
//  202ME1
//
//  Created by Jean Manas on 10/23/21.
//

import Foundation

struct Food: Item {
    var id: Int
    var name: String
    var size: Size
    
    enum Size {
        case solo(Double)
        case meal(Double)
    }
    
    var price: Double {
        switch size {
        case .solo(let p), .meal(let p): return p
        }
    }
    
    var sizeStr: String {
        switch size {
        case .solo: return "Solo"
        case .meal: return "Meal"
        }
    }
}
