//
//  Beverage.swift
//  202ME1
//
//  Created by Jean Manas on 10/23/21.
//

import Foundation

struct Beverage: Item {
    var id: Int
    var name: String
    var size: Size
    
    enum Size {
        case small(Double)
        case med(Double)
        case large(Double)
    }
    
    var price: Double {
        switch size {
        case .small(let p), .med(let p), .large(let p): return p
        }
    }
    
    var sizeStr: String {
        switch size {
        case .small: return "S"
        case .med: return "M"
        case .large: return "L"
        }
    }
}

