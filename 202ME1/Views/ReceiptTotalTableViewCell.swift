//
//  ReceiptTotalTableViewCell.swift
//  202ME1
//
//  Created by Jean Manas on 10/23/21.
//

import Foundation
import UIKit

protocol ReceiptCellDelegate: AnyObject {
    func didTapDone()
}

class ReceiptTotalTableViewCell: UITableViewCell {
    weak var delegate: ReceiptCellDelegate?
    
    @IBOutlet var totalAmount: UILabel?
    @IBOutlet var cashAmount: UILabel?
    @IBOutlet var changeAmount: UILabel?
    
    @IBOutlet var doneBtn: UIButton?
    
    @IBAction private func didTapDone() {
        delegate?.didTapDone()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        doneBtn?.layer.cornerRadius = 8
        doneBtn?.layer.cornerCurve = .continuous
    }
}
