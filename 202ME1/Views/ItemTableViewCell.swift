//
//  ItemTabltViewCell.swift
//  202ME1
//
//  Created by Jean Manas on 10/23/21.
//

import Foundation
import UIKit

extension UITableViewCell {
    static var identifier: String { return
        String(describing: self)
    }
}

class ItemTableViewCell: UITableViewCell {
    @IBOutlet var itemName: UILabel?
    @IBOutlet var itemPrice: UILabel?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        let backgroundView = UIView()
        backgroundView.backgroundColor = UIColor.systemGroupedBackground
        selectedBackgroundView = backgroundView
    }
}
