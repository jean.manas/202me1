//
//  PayTableViewCell.swift
//  202ME1
//
//  Created by Jean Manas on 10/23/21.
//

import Foundation
import UIKit

protocol PayCellDelegate: AnyObject {
    func didTapPay(cashAmount: Double)
}

class PayTableViewCell: UITableViewCell {
    weak var delegate: PayCellDelegate?
    
    @IBOutlet var cashField: UITextField?
    @IBOutlet private var payBtn: UIButton?
    
    @IBAction private func didTapPay() {
        let cash = Double(cashField?.text ?? "0")
        delegate?.didTapPay(cashAmount: cash ?? 0)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        payBtn?.layer.cornerRadius = 8
        payBtn?.layer.cornerCurve = .continuous
    }
}
