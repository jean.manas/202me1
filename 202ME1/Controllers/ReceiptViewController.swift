//
//  ReceiptViewController.swift
//  202ME1
//
//  Created by Jean Manas on 10/23/21.
//

import Foundation
import UIKit

class ReceiptViewController: UITableViewController {
    var receipt: Receipt?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.register(UINib(nibName: ItemTableViewCell.identifier, bundle: nil),
                           forCellReuseIdentifier: ItemTableViewCell.identifier)
        
    }
}

extension ReceiptViewController {
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (receipt?.items.count ?? 0) + 1
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let r = receipt else { fatalError() }
        
        guard indexPath.row == receipt?.items.count else {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: ItemTableViewCell.identifier)
                    as? ItemTableViewCell else { fatalError() }
            
            let theItem = r.items[indexPath.row]
            cell.itemName?.text = "\(theItem.name) (\(theItem.sizeStr))"
            cell.itemPrice?.text = "P\(theItem.price)"
            return cell
        }
        
        /// Total
        guard let cell = tableView.dequeueReusableCell(withIdentifier: ReceiptTotalTableViewCell.identifier) as? ReceiptTotalTableViewCell
        else { fatalError() }
        
        cell.totalAmount?.text = "P\(r.total)"
        cell.cashAmount?.text = "P\(r.cashPayment)"
        cell.changeAmount?.text = "P\(r.change)"
        
        cell.delegate = self
        
        return cell
        
    }
}

extension ReceiptViewController: ReceiptCellDelegate {
    func didTapDone() {
        navigationController?.popToRootViewController(animated: true)
    }
}
