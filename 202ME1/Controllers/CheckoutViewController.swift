//
//  CheckoutViewController.swift
//  202ME1
//
//  Created by Jean Manas on 10/23/21.
//

import Foundation
import UIKit

class CheckoutViewController: UITableViewController {
    
    private var cashAmount: Double = 0
    
    var items: [Item] = []
    
    var total: Double {
        items.reduce(0, { $0 + $1.price })
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        style()
        registerCell()
    }
    
    private func style() {
        tableView.tableFooterView = UIView()
    }
    
    private func registerCell() {
        tableView.register(UINib(nibName: ItemTableViewCell.identifier, bundle: nil), forCellReuseIdentifier: ItemTableViewCell.identifier)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destination = segue.destination as? ReceiptViewController {
            destination.receipt = Receipt(items: items, cashPayment: cashAmount, discount: 0)
        }
    }
}

extension CheckoutViewController {
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        /// Our table will look like this:
        /// [index]     [content]
        ///    [0]      Menu Item
        ///    [1]      Menu Item
        ///    [2]      Menu Item
        ///    [3]      Total amount:
        ///    [4]      Cash Payment
        
        guard indexPath.row < items.count else {
            
            /// Display total amount
            if items.count == indexPath.row {
                guard let cell = tableView.dequeueReusableCell(withIdentifier: TotalTableViewCell.identifier) as? TotalTableViewCell
                else { fatalError() }
                
                cell.totalAmountLbl?.text = "P \(total)"
                return cell
            }
            
            /// Display Cash payment field
            guard let cell = tableView.dequeueReusableCell(withIdentifier: PayTableViewCell.identifier) as? PayTableViewCell
            else { fatalError() }
            
            cell.delegate = self
            return cell
        }
        
        /// Display Menu Item cell
        guard let cell = tableView.dequeueReusableCell(withIdentifier: ItemTableViewCell.identifier) as? ItemTableViewCell
        else { fatalError() }
        
        let theItem = items[indexPath.row]
        
        cell.itemName?.text = "\(theItem.name) (\(theItem.sizeStr))"
        cell.itemPrice?.text = "P\(theItem.price)"
        
        return cell
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count + 2
    }
}

extension CheckoutViewController: PayCellDelegate {
    func didTapPay(cashAmount: Double) {
        guard cashAmount >= total else { return }
        self.cashAmount = cashAmount
        performSegue(withIdentifier: "toReceipt", sender: self)
    }
}
