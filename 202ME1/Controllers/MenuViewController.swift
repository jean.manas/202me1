//
//  MenuViewController.swift
//  202ME1
//
//  Created by Jean Manas on 10/23/21.
//

import UIKit

class MenuViewController: UIViewController {
    
    // MARK: IBOutlets and IBActions
    @IBOutlet private var tableView: UITableView?
    @IBOutlet private var itemsLabel: UILabel?
    @IBOutlet private var checkoutBtn: UIButton?
    @IBOutlet private var clearBtn: UIButton?
    
    @IBAction private func didTapCheckout() {
        performSegue(withIdentifier: "toCheckout", sender: nil)
    }
    
    @IBAction private func didTapClear() {
        items = []
        updateView()
    }
    
    // MARK: Private properties and methods

    private var items: [Item] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        style()
        
        tableView?.dataSource = self
        tableView?.delegate = self
        tableView?.register(UINib(nibName: ItemTableViewCell.identifier, bundle: nil),
                            forCellReuseIdentifier: ItemTableViewCell.identifier)
    }
    
    private func add(item: Item) {
        print("\nAdd: ")
        item.printDetails()
        items.append(item)
        
        updateView()
    }
    
    private func style() {
        checkoutBtn?.layer.cornerRadius = 8
        checkoutBtn?.layer.cornerCurve = .continuous
        clearBtn?.layer.cornerRadius = 8
        clearBtn?.layer.cornerCurve = .continuous
        
        tableView?.tableFooterView = UIView()
        tableView?.separatorStyle = .none
        
        updateView()
    }
    
    private func updateView() {
        itemsLabel?.text = "(\(items.count)) item(s) in cart"
        checkoutBtn?.isEnabled = items.count > 0
    }
    
    private func getItem(from indexPath: IndexPath) -> Item {
        let data: [Item] = indexPath.section == 0 ? Data.food : Data.beverage
        return data[indexPath.row]
    }
    
    // MARK: Overrides
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destination = segue.destination as? CheckoutViewController {
            destination.items = items
        }
    }
}

// MARK: - Delegate
extension MenuViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        add(item: getItem(from: indexPath))
    }
}

// MARK: - Data Source
extension MenuViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2 // Food and Beverage
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return Data.food.count
        }
        
        return Data.beverage.count
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section == 0 {
            return "Food"
        }
        
        return "Beverage"
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: ItemTableViewCell.identifier) as? ItemTableViewCell
        else { fatalError() }
        
        let theItem = getItem(from: indexPath)
        
        cell.itemName?.text = "\(theItem.name) (\(theItem.sizeStr))"
        cell.itemPrice?.text = "P\(theItem.price)"
        
        return cell
    }
}

